#!/bin/bash
#SBATCH -J fsc_boot
#SBATCH -e ../conOutputs/m01_b1_b2_quater_boot.err
#SBATCH -o ../conOutputs/m01_b1_b2_quater_boot.out
#SBATCH --cpus-per-task 2
#SBATCH --mem 20G
#SBATCH -p long

export PATH=$PATH:$HOME/.local/bin

#Computing likelihood of the parameters using the ECM-Brent algorithm
echo ""
fsc28 -i m01_b1_b2_quater_boot.par -n 100 -I -m --foldedSFS -c2 -x -B2 -s0 -j 

echo ""
echo "bootstraps terminated"