#!/bin/bash
#SBATCH -J angsd
#SBATCH --array=1-18
#SBATCH -e saf_exilis-%a.err
#SBATCH -o saf_exilis-%a.out
#SBATCH -p fast
#SBATCH --cpus-per-task 6
#SBATCH --mem 40G

module load angsd/0.940

REF="/shared/projects/fonio_rawdata/data/reference/04092019_Digitaria_Exilis_v1.1_pseudomolecules.fasta"

Chr=$(cat /shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/liste_chromosomes | sed -n ${SLURM_ARRAY_TASK_ID}p) 

angsd -bam bam.exilis.filelist -doSaf 1 \
-r $Chr \
-out saf_exilis_$Chr \
-GL 1 \
-P 6 \
-minMapQ 30 \
-baq 1 \
-minInd 17  \
-anc $REF \
-ref $REF