#!/bin/bash
#SBATCH -J angsd
#SBATCH -e sfsternata.err
#SBATCH -o sfsternata.out
#SBATCH -p fast
#SBATCH --cpus-per-task 6
#SBATCH --mem 50G

module load angsd/0.940

#do for all species
saf_file="/shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/merged_saf_ternata.saf.idx"
species="ternata"

realSFS $saf_file -fold 1 -P 6 > ${species}_folded.sfs