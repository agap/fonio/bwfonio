#!/bin/bash 

# Script Adapted from Laurent Excoffier February 2013 

# The script will launch several jobs of fsc28 to estimate demographic parameters from the SFS
# using a each time using a conditional maximization (ECM) of the parameter likelihood
 
## We then use the previous .est and .tpl files to estimate the parameters of the 100 pseudo-observed datasets.
## We use the --initValues option to reduce the number of runs necessary to infer parameters.
jobcount=0 
msgs=conOutputs

#-------- Number of different runs per data set ------
numRuns=100 
runBase=1
#-----------------------------

mkdir $msgs 2>/dev/null

#-------- Default run values ------
numSims=500000		#-n command line option
numCycles=50		#-L command line option
minValidSFSEntry=5	#-C command line option

#-------- Generic Name ------
genericName=m01_b1_b2_quater_boot
tplGenericName=m01_b1_b2_quater_boot 
estGenericName=m01_b1_b2_quater_boot
#-----------------------------

for dirs in $genericName
do
	#Check that dirs is a directory
	if [ -d "$dirs" ]; then
		cd $dirs
		echo "Main directory : $dirs"
		estFile=$estGenericName.est
		tplFile=$tplGenericName.tpl
		pvFile=$tplGenericName.pv 
		for (( runsDone=$runBase; runsDone<=$numRuns; runsDone++ ))
		do
			runDir="${genericName}_$runsDone"
			echo "--------------------------------------------------------------------"
			echo ""
			cd ${genericName}/$runDir
			pwd
			#Copying necessary files
			cp ../../$tplFile .
			cp ../../$estFile .
			cp ../../$pvFile  .
			let jobcount=jobcount+1
			jobName=${genericName}${jobcount}.sh
			#Creating bash file on the fly
			(
			echo "#!/bin/bash"
			echo "#SBATCH -J fsc_${genericName}_${jobcount}"
			echo "#SBATCH -e ../../../$msgs/${genericName}_${runsDone}.err"
			echo "#SBATCH -o ../../../$msgs/${genericName}_${runsDone}.out"
			echo "#SBATCH --cpus-per-task 1"
			echo "#SBATCH --mem 2G"
			echo "#SBATCH -p long"
			echo ""
			echo "#Computing likelihood of the parameters using the ECM-Brent algorithm"
			echo "echo \"\""
			echo "fsc28 -t ${genericName}.tpl -e ${genericName}.est -n $numSims -I -M -m -C ${minValidSFSEntry} -L ${numCycles} --foldedSFS -c1 -x -B1 --initValues ${genericName}.pv"
			echo ""
			echo "echo \"\""
			echo "echo \"Job $jobcount terminated\""
			) > $jobName
			chmod +x $jobName
			echo "Bash file $jobName created"
			sbatch ${jobName}
			cd ../.. 
			pwd
		done
	cd ..
	fi
done