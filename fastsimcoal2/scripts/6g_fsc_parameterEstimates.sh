#!/bin/bash 

# Script Adapted from Laurent Excoffier February 2013 

# The script will launch several jobs of fsc28 to estimate demographic parameters from the SFS
# using a each time using a conditional maximization (ECM) of the parameter likelihood
 
 
jobcount=0 
msgs=conOutputs

#-------- Number of different runs per data set ------
numRuns=100 
runBase=1
#-----------------------------

mkdir $msgs 2>/dev/null

#-------- Default run values ------
numSims=500000		#-n command line option
numCycles=50		#-L command line option
minValidSFSEntry=5	#-C command line option

#-------- Generic Name ------
model=m01
genericName=m01 
#-----------------------------

#-------- Best Run -----------
bestrun=$(cat ${model}/${model}_bestrun.txt)
#-----------------------------

#-------- Parfile-------------
parfile=${model}/${bestrun}/${model}/${model}_maxL.par
#-----------------------------

for dirs in $genericName
do
	#Check that dirs is a directory
	if [ -d "$dirs" ]; then
		cd $dirs
		rm -rf run*
		echo "Main directory : $dirs"
		for (( runsDone=$runBase; runsDone<=$numRuns; runsDone++ ))
		do
			runDir="run$runsDone"
			mkdir $runDir 2>/dev/null
			echo "--------------------------------------------------------------------"
			echo ""
			echo "Currrent file: $subDirs $runDir"
			echo ""
			cd $runDir
			#Copying necessary files
			cp ../../$parfile .
			cp ../../*.obs .
			rename 4popmodel ${genericName} *4popmodel*
			let jobcount=jobcount+1
			jobName=${genericName}${jobcount}.sh
			#Creating bash file on the fly
			(
			echo "#!/bin/bash"
			echo "#SBATCH -J fsc_${GenericName}_${jobcount}"
			echo "#SBATCH -e ../../$msgs/${GenericName}_${runsDone}.err"
			echo "#SBATCH -o ../../$msgs/${GenericName}_${runsDone}.out"
			echo "#SBATCH --cpus-per-task 1"
			echo "#SBATCH --mem 2G"
			echo "#SBATCH -p fast"
			echo ""
			echo "#Computing likelihood of the parameters using the ECM-Brent algorithm"
			echo "echo \"\""
			echo "fsc28 -i ${genericName}.par -n $numSims -I -m --foldedSFS -c1 -x -B1"
			echo ""
			echo "echo \"\""
			echo "echo \"Job $jobcount terminated\""
			) > $jobName
			chmod +x $jobName
			echo "Bash file $jobName created"
			sbatch ${jobName}
			cd .. 
		done
	cd ..
	fi
done