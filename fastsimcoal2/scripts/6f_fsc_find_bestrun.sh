#!/bin/bash


# The script will finv which run out of 100 independent runs performed for the model has the best estimated likelihood
# The best Estimated likelihood is the closest to the observed likelihood 


### Create a file with the estimated likelihood for each run 

for m in m01 m02 m03 m04 m05 m06 
	do
		cd $m
		rm ${m}_estimated_likelihoods.txt
		touch ${m}_estimated_likelihoods.txt
			for i in {1..100}
				do
					cd run$i
					cd $m
					if [ -e ${m}.bestlhoods ]; then
						echo -n "run${i}:" >> ../../${m}_estimated_likelihoods.txt
						col_name="MaxEstLhood"
						col_num=$(awk -F'\t' -v colname="$col_name" 'NR==1 { for(i=1; i<=NF; i++) if($i == colname) { print i; exit } }' ${m}.bestlhoods)
						est=$(cut -f"$col_num" -d $'\t' ${m}.bestlhoods | awk 'NR==2')
						echo $est >> ../../${m}_estimated_likelihoods.txt
						cd ../..
					else
						echo "file doest not exist"
						cd ../..
					fi
				done
		cd ..
	done


### find the best run
echo $PWD
for m in m01 m02 m03 m04 m05 m06 
	do 
		cd $m
		echo $m
		if [ -e ${m}_estimated_likelihoods.txt ]; then
			rm $m\_bestrun.txt
			rm $m\_best_likelihood.txt
			touch $m\_bestrun.txt
			touch $m\_best_likelihood.txt
			awk -F: '{ num = $2 - val; print (num < 0 ? -num : num), $1}' val=-119426542.135 $m\_estimated_likelihoods.txt | awk '{printf "%.10f %s\n", $1, $2}' | sort -n | head -n 1 | cut -d' ' -f2- > $m\_bestrun.txt
			awk -F: '{ num = $2 - val; print (num < 0 ? -num : num), $0}' val=-119426542.135 $m\_estimated_likelihoods.txt | awk '{printf "%.10f %s\n", $1, $2}' | sort -n | head -n 1 | cut -d':' -f2- > $m\_best_likelihood.txt
			cd ..
		else
			echo "file does not exist"
			cd ..
		fi
	done