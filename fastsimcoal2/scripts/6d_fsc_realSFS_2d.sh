#!/bin/bash
#SBATCH -J angsd
#SBATCH --array=1-6
#SBATCH -e 2dsfs-%a.err
#SBATCH -o 2dsfs-%a.out
#SBATCH -p long
#SBATCH --cpus-per-task 12
#SBATCH --mem 55G

module load angsd/0.940

saf_file_1="/shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/merged_saf_exilis.saf.idx"
saf_file_2="/shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/merged_saf_longiflora.saf.idx"
saf_file_3="/shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/merged_saf_iburua.saf.idx"
saf_file_4="/shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/merged_saf_ternata.saf.idx"

sfs_1="$saf file_1 $saf_file2"
sfs_2="pop2_0.obs"
sfs_3="pop3_0.obs"
sfs_4="pop2_1.obs"
sfs_5="pop3_1.obs"
sfs_6="pop3_2.obs"


### ATTENTION SCRIPT A AMELIORER. IL GENERE 6 FOIS LES 6 SFS

realSFS $saf_file_1 $saf_file_2 -fold 1 -P 12 > 4popSFS${SLURM_ARRAY_TASK_ID}_jointMAFpop1_0.obs 

realSFS $saf_file_1 $saf_file_3 -fold 1 -P 12 > 4popSFS${SLURM_ARRAY_TASK_ID}_jointMAFpop2_0.obs

realSFS $saf_file_1 $saf_file_4 -fold 1 -P 12 > 4popSFS${SLURM_ARRAY_TASK_ID}_jointMAFpop3_0.obs

realSFS $saf_file_2 $saf_file_3 -fold 1 -P 12 > 4popSFS${SLURM_ARRAY_TASK_ID}_jointMAFpop2_1.obs

realSFS $saf_file_2 $saf_file_4 -fold 1 -P 12 > 4popSFS${SLURM_ARRAY_TASK_ID}_jointMAFpop3_1.obs

realSFS $saf_file_3 $saf_file_4 -fold 1 -P 12 > 4popSFS${SLURM_ARRAY_TASK_ID}_jointMAFpop3_2.obs
