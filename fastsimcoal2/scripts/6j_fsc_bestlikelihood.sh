#!/bin/bash

GenericName="m01_b1_b2_quater_boot"

cd ${GenericName}/${GenericName}

cat ${GenericName}_1/${GenericName}/${GenericName}.bestl* > paramBoot_${GenericName}

for i in {2..100}
do
	tail -n1 ${GenericName}_${i}/${GenericName}/${GenericName}.bestl* >> paramBoot_${GenericName}		
done