#!/bin/bash
#SBATCH -J angsd
#SBATCH -e catsaf_ternata.err
#SBATCH -o catsaf_ternata.out
#SBATCH -p fast
#SBATCH --cpus-per-task 4
#SBATCH --mem 30G

module load angsd/0.940

#do for all species
species="ternata"

realSFS cat saf_${species}_Dexi_CM05836_01A.saf.idx \
saf_${species}_Dexi_CM05836_01B.saf.idx \
saf_${species}_Dexi_CM05836_02A.saf.idx \
saf_${species}_Dexi_CM05836_02B.saf.idx \
saf_${species}_Dexi_CM05836_03A.saf.idx \
saf_${species}_Dexi_CM05836_03B.saf.idx \
saf_${species}_Dexi_CM05836_04A.saf.idx \
saf_${species}_Dexi_CM05836_04B.saf.idx \
saf_${species}_Dexi_CM05836_05A.saf.idx \
saf_${species}_Dexi_CM05836_05B.saf.idx \
saf_${species}_Dexi_CM05836_06A.saf.idx \
saf_${species}_Dexi_CM05836_06B.saf.idx \
saf_${species}_Dexi_CM05836_07A.saf.idx \
saf_${species}_Dexi_CM05836_07B.saf.idx \
saf_${species}_Dexi_CM05836_08A.saf.idx \
saf_${species}_Dexi_CM05836_08B.saf.idx \
saf_${species}_Dexi_CM05836_09A.saf.idx \
saf_${species}_Dexi_CM05836_09B.saf.idx \
-outnames merged_saf_ternata \
-P 4