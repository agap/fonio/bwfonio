//Number of population samples (demes)
4 population to simulate
// Demes sizes
Nexi$
Nlongi$
Nibu$
Nterna$
//Sample sizes (followed by sampling times and inbreeding if required)
42
16
38
12
//Growth rates : negative growths implies population expansion
0
0
0
0
//Number of migration matrices: 0 implies no migration between demes
0
//Historical events: first line number of hist events; source, sink, migrants, new size|bot intensity, growth rates, migr.matrix
3 historical events
T1$ 2 3 1 1 0 0
T0$ 0 3 1 1 0 0
T2$ 1 3 1 Res1a$ 0 0
//Number of independant loci (chromosomes)
1 0
//Per chromosome: Number of linkage blocks
1
//per block: data type, num loci, rec.rate and mut rate + optional parameters
FREQ 1 0 6.5e-9
