#!/bin/bash
#SBATCH -J twisst
#SBATCH -e twisst.err
#SBATCH -o twisst.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G

#see tutorial https://github.com/simonhmartin/twisst/tree/master

module load python/3.9

script_1="/shared/projects/fonio_rawdata/results/first_dataset_VCF/twisst/twisst.py"

python $script_1 -t trees.62samples.phyml.w50.trees.gz \
-w trees.62samples.weights.csv.gz \
--outputTopos topologies.62samples.trees \
-g D.exilis \
-g D.longiflora \
-g D.iburua \
-g D.ternata \
-g D.ternata_IC \
-g D.longiflora_east \
--method complete --groupsFile groupsphased2_62samples.tsv