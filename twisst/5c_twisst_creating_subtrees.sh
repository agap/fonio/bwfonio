#!/bin/bash
#SBATCH -J subtrees
#SBATCH -e subtrees.err
#SBATCH -o subtrees.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G


#see tutorial https://github.com/simonhmartin/twisst/tree/master


module load python/2.7

script_1="/shared/projects/fonio_rawdata/results/first_dataset_VCF/twisst/parseVCF.py"

input="/shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/all_chr_twisst_62samples_RemoveSite05missing_fixed_phased.vcf.gz"

## Parse VCF files

python $script_1 -i $input | gzip > trees_62samples.geno.gz

## trees in windows

module load phyml/3.3.20190909

script_2="/shared/projects/fonio_rawdata/results/first_dataset_VCF/twisst/phyml_sliding_windows.py"

python $script_2 -T 10 -g trees_62samples.geno.gz --prefix trees.62samples.phyml.w50 -w 50 --windType sites --model GTR --optimise n
