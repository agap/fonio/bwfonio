#!/bin/bash
#SBATCH -J vcftools
#SBATCH -e vcftoolstwisst.err
#SBATCH -o vcftoolstwisst.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 5G

module load vcftools/0.1.16
module load samtools/1.14

#see tutorial https://github.com/simonhmartin/twisst/tree/master

##### VCF
vcf_file="/shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/all_chr_first_dataset_247samples_FILTERED_masking2DP_RemoveSite05missing.vcf.gz"

### files that contain the ID of individuals kept

file="/shared/projects/fonio_rawdata/source/twisstInd2"

#### Generate new vcf ####

vcftools --gzvcf $vcf_file --keep $file \
--recode --recode-INFO-all --stdout | bgzip > all_chr_twisst_62samples.vcf.gz

tabix -f -p vcf all_chr_twisst_62samples.vcf.gz

#### Remove non polymorphic sites and keep sites present at least in 80% of the individuals ####

module load bcftools/1.15.1

bcftools view -e 'F_MISSING>=0.05 || (COUNT(GT="AA")+COUNT(GT="mis"))=N_SAMPLES || (COUNT(GT="RR")+COUNT(GT="mis"))=N_SAMPLES || (COUNT(GT="AR")+COUNT(GT="mis"))=N_SAMPLES' \
--threads 10 \
-Oz \
-o all_chr_twisst_62samples_RemoveSite05missing.vcf.gz \
all_chr_twisst_62samples.vcf.gz

tabix -f -p vcf all_chr_twisst_62samples_RemoveSite05missing.vcf.gz

#### Fix the INFO fields ####

module load vcflib/1.0.3

vcffixup all_chr_twisst_62samples_RemoveSite05missing.vcf.gz | bgzip > all_chr_twisst_62samples_RemoveSite05missing_fixed.vcf.gz