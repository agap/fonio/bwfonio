#!/bin/bash
#SBATCH -J beagle
#SBATCH -e beagle.err
#SBATCH -o beagle.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G


#see tutorial https://github.com/simonhmartin/twisst/tree/master


module load java-jdk/8.0.112

input="/shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/all_chr_twisst_62samples_RemoveSite05missing_fixed"

java -Xmx12g -jar beagle.r1399.jar gt=${input}.vcf.gz out=${input}_phased impute=true nthreads=20 window=10000 overlap=1000 gprobs=false