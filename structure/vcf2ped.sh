#!/bin/bash
#SBATCH -J vcf2ped
#SBATCH -e vcf2ped.err
#SBATCH -o vcf2ped.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 20G

module load vcftools/0.1.16

file="/shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/all_chr_first_dataset_247samples_FILTERED_masking2DP_RemoveSite05missing_005MAF"

vcftools --gzvcf $file\.vcf.gz \
--plink --chrom-map /shared/projects/fonio_rawdata/source/chrom-map.txt --out $file