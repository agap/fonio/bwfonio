#!/bin/bash
#SBATCH -J egglib
#SBATCH -e egglib_diffstat_species.err
#SBATCH -o egglib_diffstat_species.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 8
#SBATCH --mem 80G

module load python/3.9

python /shared/projects/fonio_rawdata/results/first_dataset_VCF/egglib/diffStats_geneticClusters_247ID_05missing.py > diffStats_247ID_geneticClusters_05missing.txt