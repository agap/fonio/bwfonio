import egglib

#### Import the vcf ####
vcf = egglib.io.VCF("/shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/all_chr_first_dataset_247samples_FILTERED_masking2DP_RemoveSite05missing.bcf.gz")
print(vcf.get_samples())
print(vcf.has_index)
print(vcf.num_samples) 

#### read the vcf ####
vcf.read()
print(vcf.get_chrom(), vcf.get_pos())  
print(vcf.get_infos())
print(vcf.get_alleles())
print(vcf.is_snp())


#define the structure with two levels: species and individual
#p1 = D. longiflora, p2 = D. exilis, p3 = D. iburua, p4 = D. ternata
labels = [('p1', 'i1'), ('p1', 'i1'), ('p4', 'i2'), ('p4', 'i2'), ('p4', 'i3'), ('p4', 'i3'), ('p2', 'i4'), ('p2', 'i4'), ('p2', 'i5'), ('p2', 'i5'), ('p2', 'i6'), ('p2', 'i6'), ('p2', 'i7'), ('p2', 'i7'), ('p2', 'i8'), ('p2', 'i8'), ('p2', 'i9'), ('p2', 'i9'), ('p2', 'i10'), ('p2', 'i10'), ('p2', 'i11'), ('p2', 'i11'), ('p2', 'i12'), ('p2', 'i12'), ('p2', 'i13'), ('p2', 'i13'), ('p2', 'i14'), ('p2', 'i14'), ('p2', 'i15'), ('p2', 'i15'), ('p2', 'i16'), ('p2', 'i16'), ('p2', 'i17'), ('p2', 'i17'), ('p2', 'i18'), ('p2', 'i18'), ('p2', 'i19'), ('p2', 'i19'), ('p2', 'i20'), ('p2', 'i20'), ('p2', 'i21'), ('p2', 'i21'), ('p2', 'i22'), ('p2', 'i22'), ('p2', 'i23'), ('p2', 'i23'), ('p2', 'i24'), ('p2', 'i24'), ('p2', 'i25'), ('p2', 'i25'), ('p2', 'i26'), ('p2', 'i26'), ('p2', 'i27'), ('p2', 'i27'), ('p2', 'i28'), ('p2', 'i28'), ('p2', 'i29'), ('p2', 'i29'), ('p2', 'i30'), ('p2', 'i30'), ('p2', 'i31'), ('p2', 'i31'), ('p2', 'i32'), ('p2', 'i32'), ('p2', 'i33'), ('p2', 'i33'), ('p2', 'i34'), ('p2', 'i34'), ('p2', 'i35'), ('p2', 'i35'), ('p2', 'i36'), ('p2', 'i36'), ('p2', 'i37'), ('p2', 'i37'), ('p2', 'i38'), ('p2', 'i38'), ('p2', 'i39'), ('p2', 'i39'), ('p2', 'i40'), ('p2', 'i40'), ('p2', 'i41'), ('p2', 'i41'), ('p2', 'i42'), ('p2', 'i42'), ('p2', 'i43'), ('p2', 'i43'), ('p2', 'i44'), ('p2', 'i44'), ('p2', 'i45'), ('p2', 'i45'), ('p2', 'i46'), ('p2', 'i46'), ('p2', 'i47'), ('p2', 'i47'), ('p2', 'i48'), ('p2', 'i48'), ('p2', 'i49'), ('p2', 'i49'), ('p2', 'i50'), ('p2', 'i50'), ('p2', 'i51'), ('p2', 'i51'), ('p2', 'i52'), ('p2', 'i52'), ('p2', 'i53'), ('p2', 'i53'), ('p2', 'i54'), ('p2', 'i54'), ('p2', 'i55'), ('p2', 'i55'), ('p2', 'i56'), ('p2', 'i56'), ('p2', 'i57'), ('p2', 'i57'), ('p2', 'i58'), ('p2', 'i58'), ('p2', 'i59'), ('p2', 'i59'), ('p2', 'i60'), ('p2', 'i60'), ('p2', 'i61'), ('p2', 'i61'), ('p2', 'i62'), ('p2', 'i62'), ('p2', 'i63'), ('p2', 'i63'), ('p2', 'i64'), ('p2', 'i64'), ('p2', 'i65'), ('p2', 'i65'), ('p2', 'i66'), ('p2', 'i66'), ('p2', 'i67'), ('p2', 'i67'), ('p2', 'i68'), ('p2', 'i68'), ('p2', 'i69'), ('p2', 'i69'), ('p2', 'i70'), ('p2', 'i70'), ('p2', 'i71'), ('p2', 'i71'), ('p2', 'i72'), ('p2', 'i72'), ('p2', 'i73'), ('p2', 'i73'), ('p2', 'i74'), ('p2', 'i74'), ('p2', 'i75'), ('p2', 'i75'), ('p2', 'i76'), ('p2', 'i76'), ('p2', 'i77'), ('p2', 'i77'), ('p2', 'i78'), ('p2', 'i78'), ('p2', 'i79'), ('p2', 'i79'), ('p2', 'i80'), ('p2', 'i80'), ('p2', 'i81'), ('p2', 'i81'), ('p2', 'i82'), ('p2', 'i82'), ('p2', 'i83'), ('p2', 'i83'), ('p2', 'i84'), ('p2', 'i84'), ('p2', 'i85'), ('p2', 'i85'), ('p2', 'i86'), ('p2', 'i86'), ('p2', 'i87'), ('p2', 'i87'), ('p2', 'i88'), ('p2', 'i88'), ('p2', 'i89'), ('p2', 'i89'), ('p2', 'i90'), ('p2', 'i90'), ('p2', 'i91'), ('p2', 'i91'), ('p2', 'i92'), ('p2', 'i92'), ('p2', 'i93'), ('p2', 'i93'), ('p2', 'i94'), ('p2', 'i94'), ('p2', 'i95'), ('p2', 'i95'), ('p2', 'i96'), ('p2', 'i96'), ('p2', 'i97'), ('p2', 'i97'), ('p2', 'i98'), ('p2', 'i98'), ('p2', 'i99'), ('p2', 'i99'), ('p2', 'i100'), ('p2', 'i100'), ('p2', 'i101'), ('p2', 'i101'), ('p2', 'i102'), ('p2', 'i102'), ('p2', 'i103'), ('p2', 'i103'), ('p2', 'i104'), ('p2', 'i104'), ('p2', 'i105'), ('p2', 'i105'), ('p2', 'i106'), ('p2', 'i106'), ('p2', 'i107'), ('p2', 'i107'), ('p2', 'i108'), ('p2', 'i108'), ('p2', 'i109'), ('p2', 'i109'), ('p2', 'i110'), ('p2', 'i110'), ('p2', 'i111'), ('p2', 'i111'), ('p2', 'i112'), ('p2', 'i112'), ('p2', 'i113'), ('p2', 'i113'), ('p2', 'i114'), ('p2', 'i114'), ('p2', 'i115'), ('p2', 'i115'), ('p2', 'i116'), ('p2', 'i116'), ('p2', 'i117'), ('p2', 'i117'), ('p2', 'i118'), ('p2', 'i118'), ('p2', 'i119'), ('p2', 'i119'), ('p2', 'i120'), ('p2', 'i120'), ('p2', 'i121'), ('p2', 'i121'), ('p2', 'i122'), ('p2', 'i122'), ('p2', 'i123'), ('p2', 'i123'), ('p2', 'i124'), ('p2', 'i124'), ('p2', 'i125'), ('p2', 'i125'), ('p2', 'i126'), ('p2', 'i126'), ('p2', 'i127'), ('p2', 'i127'), ('p2', 'i128'), ('p2', 'i128'), ('p2', 'i129'), ('p2', 'i129'), ('p2', 'i130'), ('p2', 'i130'), ('p2', 'i131'), ('p2', 'i131'), ('p2', 'i132'), ('p2', 'i132'), ('p2', 'i133'), ('p2', 'i133'), ('p2', 'i134'), ('p2', 'i134'), ('p2', 'i135'), ('p2', 'i135'), ('p2', 'i136'), ('p2', 'i136'), ('p2', 'i137'), ('p2', 'i137'), ('p2', 'i138'), ('p2', 'i138'), ('p2', 'i139'), ('p2', 'i139'), ('p2', 'i140'), ('p2', 'i140'), ('p2', 'i141'), ('p2', 'i141'), ('p2', 'i142'), ('p2', 'i142'), ('p2', 'i143'), ('p2', 'i143'), ('p2', 'i144'), ('p2', 'i144'), ('p2', 'i145'), ('p2', 'i145'), ('p2', 'i146'), ('p2', 'i146'), ('p2', 'i147'), ('p2', 'i147'), ('p2', 'i148'), ('p2', 'i148'), ('p2', 'i149'), ('p2', 'i149'), ('p2', 'i150'), ('p2', 'i150'), ('p2', 'i151'), ('p2', 'i151'), ('p2', 'i152'), ('p2', 'i152'), ('p2', 'i153'), ('p2', 'i153'), ('p2', 'i154'), ('p2', 'i154'), ('p2', 'i155'), ('p2', 'i155'), ('p2', 'i156'), ('p2', 'i156'), ('p2', 'i157'), ('p2', 'i157'), ('p2', 'i158'), ('p2', 'i158'), ('p2', 'i159'), ('p2', 'i159'), ('p2', 'i160'), ('p2', 'i160'), ('p2', 'i161'), ('p2', 'i161'), ('p2', 'i162'), ('p2', 'i162'), ('p2', 'i163'), ('p2', 'i163'), ('p2', 'i164'), ('p2', 'i164'), ('p1', 'i165'), ('p1', 'i165'), ('p1', 'i166'), ('p1', 'i166'), ('p2', 'i167'), ('p2', 'i167'), ('p1', 'i168'), ('p1', 'i168'), ('p1', 'i169'), ('p1', 'i169'), ('p1', 'i170'), ('p1', 'i170'), ('p1', 'i171'), ('p1', 'i171'), ('p1', 'i172'), ('p1', 'i172'), ('p1', 'i173'), ('p1', 'i173'), ('p1', 'i174'), ('p1', 'i174'), ('p2', 'i175'), ('p2', 'i175'), ('p2', 'i176'), ('p2', 'i176'), ('p1', 'i177'), ('p1', 'i177'), ('p1', 'i178'), ('p1', 'i178'), ('p1', 'i179'), ('p1', 'i179'), ('p1', 'i180'), ('p1', 'i180'), ('p2', 'i181'), ('p2', 'i181'), ('p2', 'i182'), ('p2', 'i182'), ('p2', 'i183'), ('p2', 'i183'), ('p2', 'i184'), ('p2', 'i184'), ('p2', 'i185'), ('p2', 'i185'), ('p2', 'i186'), ('p2', 'i186'), ('p2', 'i187'), ('p2', 'i187'), ('p2', 'i188'), ('p2', 'i188'), ('p3', 'i189'), ('p3', 'i189'), ('p3', 'i190'), ('p3', 'i190'), ('p3', 'i191'), ('p3', 'i191'), ('p3', 'i192'), ('p3', 'i192'), ('p3', 'i193'), ('p3', 'i193'), ('p3', 'i194'), ('p3', 'i194'), ('p3', 'i195'), ('p3', 'i195'), ('p3', 'i196'), ('p3', 'i196'), ('p3', 'i197'), ('p3', 'i197'), ('p3', 'i198'), ('p3', 'i198'), ('p3', 'i199'), ('p3', 'i199'), ('p3', 'i200'), ('p3', 'i200'), ('p3', 'i201'), ('p3', 'i201'), ('p3', 'i202'), ('p3', 'i202'), ('p2', 'i203'), ('p2', 'i203'), ('p2', 'i204'), ('p2', 'i204'), ('p4', 'i205'), ('p4', 'i205'), ('p4', 'i206'), ('p4', 'i206'), ('p4', 'i207'), ('p4', 'i207'), ('p4', 'i208'), ('p4', 'i208'), ('p4', 'i209'), ('p4', 'i209'), ('p4', 'i210'), ('p4', 'i210'), ('p4', 'i211'), ('p4', 'i211'), ('p4', 'i212'), ('p4', 'i212'), ('p4', 'i213'), ('p4', 'i213'), ('p2', 'i214'), ('p2', 'i214'), ('p2', 'i215'), ('p2', 'i215'), ('p2', 'i216'), ('p2', 'i216'), ('p2', 'i217'), ('p2', 'i217'), ('p2', 'i218'), ('p2', 'i218'), ('p2', 'i219'), ('p2', 'i219'), ('p3', 'i220'), ('p3', 'i220'), ('p2', 'i221'), ('p2', 'i221'), ('p2', 'i222'), ('p2', 'i222'), ('p2', 'i223'), ('p2', 'i223'), ('p2', 'i224'), ('p2', 'i224'), ('p2', 'i225'), ('p2', 'i225'), ('p3', 'i226'), ('p3', 'i226'), ('p3', 'i227'), ('p3', 'i227'), ('p2', 'i228'), ('p2', 'i228'), ('p2', 'i229'), ('p2', 'i229'), ('p3', 'i230'), ('p3', 'i230'), ('p3', 'i231'), ('p3', 'i231'), ('p3', 'i232'), ('p3', 'i232'), ('p2', 'i233'), ('p2', 'i233'), ('p3', 'i234'), ('p3', 'i234'), ('p3', 'i235'), ('p3', 'i235'), ('p2', 'i236'), ('p2', 'i236'), ('p2', 'i237'), ('p2', 'i237'), ('p2', 'i238'), ('p2', 'i238'), ('p2', 'i239'), ('p2', 'i239'), ('p2', 'i240'), ('p2', 'i240'), ('p2', 'i241'), ('p2', 'i241'), ('p2', 'i242'), ('p2', 'i242'), ('p2', 'i243'), ('p2', 'i243'), ('p2', 'i244'), ('p2', 'i244'), ('p2', 'i245'), ('p2', 'i245'), ('p2', 'i246'), ('p2', 'i246'), ('p2', 'i247'), ('p2', 'i247')]

struct = egglib.struct_from_iterable(labels, fmt='PI')
print(struct)
print(struct.get_populations())

#### Create a ComputeStats ####


#### subset samples to compute Pi by populations ####

### for D. longiflora 

pop1 = struct.subset(pops=['p1'])
print(pop1)
cs_pop1 = egglib.stats.ComputeStats() #create a comput stats
cs_pop1.add_stats('Pi','thetaW','D','Ho','He','R',
            'nseff','lseff','S','Ss')
cs_pop1.configure(struct=pop1)
vcf.goto('Dexi_CM05836_01A') # on part du début
sites=[] #initialisation d'une liste qui contiendra une liste de 'Site' instance
while vcf.read():
    genotypes = vcf.get_genotypes() #extract genotypes
    genotypes=[j for i in genotypes for j in i] #flatten the list
    genotypes=['-' if v is None else v for v in genotypes] #replace missing data by '-'
    site=egglib.site_from_list(genotypes,alphabet = egglib.alphabets.DNA)
    sites.append(site)

stats=cs_pop1.process_sites(sites)
print(stats)

### for D. exilis 
pop2 = struct.subset(pops=['p2'])
print(pop2)
cs_pop2 = egglib.stats.ComputeStats() #create a comput stats
cs_pop2.add_stats('Pi','thetaW','D','Ho','He','R',
            'nseff','lseff','S','Ss')
cs_pop2.configure(struct=pop2)
vcf.goto('Dexi_CM05836_01A') # on part du début
sites=[] #initialisation d'une liste qui contiendra une liste de 'Site' instance
while vcf.read():
    genotypes = vcf.get_genotypes() #extract genotypes
    genotypes=[j for i in genotypes for j in i] #flatten the list
    genotypes=['-' if v is None else v for v in genotypes] #replace missing data by '-'
    site=egglib.site_from_list(genotypes,alphabet = egglib.alphabets.DNA)
    sites.append(site)

stats=cs_pop2.process_sites(sites)
print(stats)

### for D. iburua 
pop3 = struct.subset(pops=['p3'])
print(pop3)
cs_pop3 = egglib.stats.ComputeStats() #create a comput stats
cs_pop3.add_stats('Pi','thetaW','D','Ho','He','R',
            'nseff','lseff','S','Ss')
cs_pop3.configure(struct=pop3)
vcf.goto('Dexi_CM05836_01A') # on part du début
sites=[] #initialisation d'une liste qui contiendra une liste de 'Site' instance
while vcf.read():
    genotypes = vcf.get_genotypes() #extract genotypes
    genotypes=[j for i in genotypes for j in i] #flatten the list
    genotypes=['-' if v is None else v for v in genotypes] #replace missing data by '-'
    site=egglib.site_from_list(genotypes,alphabet = egglib.alphabets.DNA)
    sites.append(site)

stats=cs_pop3.process_sites(sites)
print(stats)

### for D. ternata
pop4 = struct.subset(pops=['p4'])
print(pop4)
cs_pop4 = egglib.stats.ComputeStats() #create a comput stats
cs_pop4.add_stats('Pi','thetaW','D','Ho','He','R',
            'nseff','lseff','S','Ss')
cs_pop4.configure(struct=pop4)
vcf.goto('Dexi_CM05836_01A') # on part du début
sites=[] #initialisation d'une liste qui contiendra une liste de 'Site' instance
while vcf.read():
    genotypes = vcf.get_genotypes() #extract genotypes
    genotypes=[j for i in genotypes for j in i] #flatten the list
    genotypes=['-' if v is None else v for v in genotypes] #replace missing data by '-'
    site=egglib.site_from_list(genotypes,alphabet = egglib.alphabets.DNA)
    sites.append(site)

stats=cs_pop4.process_sites(sites)
print(stats)
