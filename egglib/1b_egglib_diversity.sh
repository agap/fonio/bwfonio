#!/bin/bash
#SBATCH -J egglib20
#SBATCH -e egglib_diversity_species_05.err
#SBATCH -o egglib_divrsity_species_05.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 8
#SBATCH --mem 80G

module load python/3.9

python /shared/projects/fonio_rawdata/results/first_dataset_VCF/egglib/diversity_stats_species_247ID_05missing.py > diversity_stats_247ID_05missing.txt