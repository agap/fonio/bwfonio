#!/bin/bash
#SBATCH -J bcftools
#SBATCH -e bcftools.err
#SBATCH -o bcftools.out
#SBATCH -p fast
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G

module load bcftools/1.15.1

cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/

file="all_chr_first_dataset_247samples_FILTERED_masking2DP_RemoveSite05missing"

bcftools convert -O b ${file}.vcf.gz > ${file}.bcf.gz
bcftools index ${file}.bcf.gz