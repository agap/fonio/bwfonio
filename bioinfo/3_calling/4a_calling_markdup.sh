#!/bin/bash
#SBATCH -J markdup
#SBATCH --array=1-79
#SBATCH -e markdup1-%a.err
#SBATCH -o markdup1-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 25G

module load gatk4/4.2.6.1 

file=$(cat /shared/projects/fonio_rawdata/results/mapping_stats/myaccessions.csv | sed -n ${SLURM_ARRAY_TASK_ID}p)
mkdir -p /shared/projects/fonio_rawdata/results/markdup/$file

INPUT="/shared/projects/fonio_rawdata/results/samview/$file\/*.mappedpaired.bam"

gatk MarkDuplicates -I $INPUT -M /shared/projects/fonio_rawdata/results/markdup/$file\/duplicates.$file\.metrics -O /shared/projects/fonio_rawdata/results/markdup/$file\/$file\.SORTED.MD.bam

## indexing
module load samtools/1.14
samtools index /shared/projects/fonio_rawdata/results/markdup/$file\/*.MD.bam 