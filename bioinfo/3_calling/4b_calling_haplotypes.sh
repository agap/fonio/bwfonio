#!/bin/bash
#SBATCH -J HC1A
#SBATCH --array=1-79
#SBATCH -e HC_Chr1A-%a.err
#SBATCH -o HC_Chr1A-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 16
#SBATCH --mem 30G

module load gatk4/4.2.6.1 

file=$(cat /shared/projects/fonio_rawdata/results/markdup/myaccessions | sed -n ${SLURM_ARRAY_TASK_ID}p)
mkdir -p /shared/projects/fonio_rawdata/results/VCF/Chr1A
mkdir -p /shared/projects/fonio_rawdata/results/VCF/Chr1A/$file

INPUT="/shared/projects/fonio_rawdata/results/markdup/$file\/*.MD.bam"
REF="/shared/projects/fonio_rawdata/data/reference/04092019_Digitaria_Exilis_v1.1_pseudomolecules.fasta"

## Per Chr: do be done for all Chr
gatk HaplotypeCaller --java-options "-Xmx19G" -ERC GVCF -I $INPUT -O /shared/projects/fonio_rawdata/results/VCF/Chr1A/$file\/$file\_Chr1A.gvcf.gz -R $REF --output-mode EMIT_ALL_ACTIVE_SITES -L Dexi_CM05836_01A