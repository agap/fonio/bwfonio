#!/bin/bash
#SBATCH -J DB1A
#SBATCH -e DB_Chr1A.err
#SBATCH -o DB_Chr1A.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 50G

module load gatk4/4.2.6.1 

gatk --java-options "-Xmx140g" GenomicsDBImport \
--sample-name-map /shared/projects/fonio_rawdata/source/DBImport_samples_Chr1A \
--genomicsdb-workspace-path /shared/projects/fonio_rawdata/results/GenomicsDB/first_dataset/mydbChr1A \
-imr OVERLAPPING_ONLY \
--batch-size 171 \
-L Dexi_CM05836_01A