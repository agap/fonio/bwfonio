#!/bin/bash
#SBATCH -J vcfminDP
#SBATCH --array=1-19
#SBATCH -e vcfminDP-%a.err
#SBATCH -o vcfminDP-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 20G

module load vcftools/0.1.16
module load samtools/1.14

file=$(cat /shared/projects/fonio_rawdata/source/list_Chr | sed -n ${SLURM_ARRAY_TASK_ID}p)

cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/$file

vcftools --gzvcf $file\_first_dataset_250samples_onlySNP_biallelic_VF_QualDp2ClustQdFsSorMqMqrksRdposrks_FILTERED.vcf.gz --minDP 2 \
--recode --recode-INFO-all --stdout | bgzip > $file\_first_dataset_250samples_onlySNP_biallelic_VF_QualDp2ClustQdFsSorMqMqrksRdposrks_FILTERED_masking2DP.vcf.gz

tabix -f -p vcf $file\_first_dataset_250samples_onlySNP_biallelic_VF_QualDp2ClustQdFsSorMqMqrksRdposrks_FILTERED_masking2DP.vcf.gz