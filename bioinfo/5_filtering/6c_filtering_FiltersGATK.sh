#!/bin/bash
#SBATCH -J gatkVF
#SBATCH --array=1-19
#SBATCH -e gatkVF-%a.err
#SBATCH -o gatkVF-%a.out
#SBATCH -p long 
#SBATCH --cpus-per-task 6
#SBATCH --mem 50G

module load gatk4/4.2.6.1
 
REF="/shared/projects/fonio_rawdata/data/reference/04092019_Digitaria_Exilis_v1.1_pseudomolecules.fasta"

file=$(cat /shared/projects/fonio_rawdata/source/list_Chr | sed -n ${SLURM_ARRAY_TASK_ID}p)

cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/$file

gatk VariantFiltration --filter-expression "QUAL<30" --filter-name "LOW_QUAL" \
--filter-expression "DP<500.0" --filter-name "LOW_DP" \
--filter-expression "DP>15000.0" --filter-name "HIGH_DP" \
--cluster-size 3 --cluster-window-size 10 \
--filter-expression "QD<2.0" --filter-name "LOW_QD" \
--filter-expression "FS>60.0" --filter-name "HIGH_FS" \
--filter-expression "SOR>3.0" --filter-name "HIGH_SOR" \
--filter-expression "MQ<40.0" --filter-name "LOW_MQ" \
--filter-expression "MQRankSum<-12.50" --filter-name "LOW_MQRkS" \
--filter-expression "ReadPosRankSum<-8.0" --filter-name "LOW_ReadPosRkS" \
-R $REF \
-V $file\_first_dataset_250samples_onlySNP_biallelic.vcf.gz \
-O $file\_first_dataset_250samples_onlySNP_biallelic_VF_QualDp2ClustQdFsSorMqMqrksRdposrks.vcf.gz

echo -e "Finished"