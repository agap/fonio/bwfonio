#!/bin/bash
#SBATCH -J vcfmaf
#SBATCH -e vcfmaf.err
#SBATCH -o vcfmaf.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G

module load vcftools/0.1.16
module load samtools/1.14
 

#on the entire vcf, previously produced by aggregating the chr.vcf
file="/shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/all_chr_first_dataset_250samples_FILTERED_masking2DP_RemoveSite05missing.vcf.gz"

vcftools --gzvcf $file --maf 0.05 --recode --recode-INFO-all --stdout | bgzip > all_chr_first_dataset_247samples_FILTERED_masking2DP_RemoveSite05missing_MAF005.vcf.gz

tabix -f -p vcf all_chr_first_dataset_247samples_FILTERED_masking2DP_RemoveSite05missing_MAF005.vcf.gz