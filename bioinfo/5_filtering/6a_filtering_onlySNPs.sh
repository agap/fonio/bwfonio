#!/bin/bash
#SBATCH -J selectSNPs
#SBATCH --array=1-19
#SBATCH -e SelSNPs-%a.err
#SBATCH -o SelSNPs-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 20G

module load gatk4/4.2.6.1
 
REF="/shared/projects/fonio_rawdata/data/reference/04092019_Digitaria_Exilis_v1.1_pseudomolecules.fasta"

file=$(cat /shared/projects/fonio_rawdata/source/list_Chr | sed -n ${SLURM_ARRAY_TASK_ID}p)

cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/$file

gatk SelectVariants -R $REF \
-V $file\_first_dataset_250samples.vcf.gz -select-type SNP \
-O $file\_first_dataset_250samples_onlySNP.vcf.gz