#!/bin/bash
#SBATCH -J bcftools
#SBATCH --array=1-19
#SBATCH -e bcfview05miss-%a.err
#SBATCH -o bcfview05miss-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 6
#SBATCH --mem 20G

module load bcftools/1.15.1
module load samtools/1.14

file=$(cat /shared/projects/fonio_rawdata/source/list_Chr | sed -n ${SLURM_ARRAY_TASK_ID}p)

cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/$file

bcftools view -e 'F_MISSING>=0.05' \
--threads 10 \
-Oz \
-o $file\_first_dataset_250samples_onlySNP_biallelic_VF_QualDp2ClustQdFsSorMqMqrksRdposrks_FILTERED_masking2DP_RemoveSite05missing.vcf.gz \
$file\_first_dataset_250samples_onlySNP_biallelic_VF_QualDp2ClustQdFsSorMqMqrksRdposrks_FILTERED_masking2DP.vcf.gz

tabix -f -p vcf $file\_first_dataset_250samples_onlySNP_biallelic_VF_QualDp2ClustQdFsSorMqMqrksRdposrks_FILTERED_masking2DP_RemoveSite05missing.vcf.gz