#!/bin/bash
#SBATCH -J GenoGVCF_1A
#SBATCH -e GenoGVCF_Chr1A.err
#SBATCH -o GenoGVCF_Chr1A.out
#SBATCH -p long 
#SBATCH --cpus-per-task 4
#SBATCH --mem 30G

module load gatk4/4.2.6.1 
REF="/shared/projects/fonio_rawdata/data/reference/04092019_Digitaria_Exilis_v1.1_pseudomolecules.fasta"

echo -e "Creating Chromosome directory"
mkdir -p /shared/projects/fonio_rawdata/results/first_dataset_VCF/Chr1A
echo -e "Directory created!"

echo -e "Genotyping is starting"
gatk --java-options "-Xmx90g" GenotypeGVCFs \
-R $REF \
-V gendb:///shared/projects/fonio_rawdata/results/GenomicsDB/first_dataset/mydbChr1A \
-O /shared/projects/fonio_rawdata/results/first_dataset_VCF/Chr1A/Chr1A_first_dataset_250samples.vcf.gz
echo -e "Finished!"