#!/bin/bash
#SBATCH -J sortsam
#SBATCH --array=1-96
#SBATCH -e sortsam-%a.err
#SBATCH -o sortsam-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G

module load picard/2.23.5

file=$(ls /shared/projects/fonio_rawdata/results/cutadapt/ | sed -n ${SLURM_ARRAY_TASK_ID}p)

echo -e "creating directory for $file"
mkdir -p /shared/projects/fonio_rawdata/results/sortsam/$file
cd $file

INPUT="/shared/projects/fonio_rawdata/results/mapping/$file\/*.sam"
echo $INPUT

picard SortSam --SORT_ORDER coordinate --VALIDATION_STRINGENCY SILENT --CREATE_INDEX TRUE --INPUT $INPUT --OUTPUT /shared/projects/fonio_rawdata/results/sortsam/$file\/$file\.SORTSAM.bam