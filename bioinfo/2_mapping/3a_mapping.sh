#!/bin/bash
#SBATCH -J bwamem
#SBATCH --array=1-96
#SBATCH -e bwamem-%a.err
#SBATCH -o bwamem-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 6
#SBATCH --mem 50G

module load bwa-mem2/2.2.1

file=$(ls /shared/projects/fonio_rawdata/results/cutadapt/ | sed -n ${SLURM_ARRAY_TASK_ID}p)

echo -e "creating directory for $file"
mkdir -p /shared/projects/fonio_rawdata/results/mapping/$file
cd $file

echo -e "declaration des variables"
REF="/shared/projects/fonio_rawdata/data/reference/04092019_Digitaria_Exilis_v1.1_pseudomolecules.fasta"
READS_R1="/shared/projects/fonio_rawdata/results/cutadapt/$file\/*_1.fastq.gz"
READS_R2="/shared/projects/fonio_rawdata/results/cutadapt/$file\/*_2.fastq.gz"
echo -e $READS_R1 $READS_R2
echo $REF

echo -e "mapping"
bwa-mem2 mem -R "@RG\tID:$file\tSM:$file\tPL:Illumina" -M -t $SLURM_CPUS_PER_TASK "$REF" $READS_R1 $READS_R2 -o $file.sam 
echo -e "finished"