#!/bin/bash
#SBATCH -J samview
#SBATCH --array=1-96
#SBATCH -e samview-%a.err
#SBATCH -o samview-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 1
#SBATCH --mem 10G

module load samtools/1.14 

file=$(ls /shared/projects/fonio_rawdata/results/cutadapt/ | sed -n ${SLURM_ARRAY_TASK_ID}p)

echo -e "creating directory for $file"
mkdir -p /shared/projects/fonio_rawdata/results/samview/$file
cd $file

INPUT="/shared/projects/fonio_rawdata/results/sortsam/$file\/*.SORTSAM.bam"
echo $INPUT

samtools view -b -h -f 0x02 -@4 -o $file\.mappedpaired.bam $INPUT

### Indexing
samtools index *.mappedpaired.bam