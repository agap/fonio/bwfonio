#! /usr/bin/env perl

###################
#
# Copyright 2012-2018 IRD
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/> or
# write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# You should have received a copy of the CeCILL-C license with this program.
#If not see <http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.txt>
#
# Intellectual property belongs to IRD
#
# Written by Francois Sabot 
#
#################### 


use strict;
use Getopt::Long;

#For gz files
use IO::Compress::Gzip qw(gzip $GzipError);
use IO::Uncompress::Gunzip qw(gunzip $GunzipError);

my $courriel="francois.sabot-at-ird.fr";
my ($nomprog) = $0 =~/([^\/]+)$/;
my $MessAbruti ="\nUsage:
\t$nomprog -f forward_seq -r reverse_seq [-o outputPrefix]
or
\t$nomprog -forward forward_seq -reverse reverse_seq [-out outputPrefix]

DESCRIPTION

From a forward and a reverse fastq files unpaired, it will generate three new files named [prefix]_1.fastq, [prefix]_2.fastq and [prefix]_single.fastq.

If no prefix is signified, it will be the text in the name of the forward file before the first '_' or '.' added of 'Repaired' (Example tutu_1.fastq will give tutuRepaired).

Fastq files can be plained or gzipped. Output files will be of the same type.


	contact: $courriel\n\n";
	

unless (@ARGV) 
	{
	print "\nType --help for more informations\n\n";
	exit;
	}

my ($forward, $reverse,$prefix,$help);

GetOptions("help|?|h" => \$help,
			"f|forward=s"=>\$forward,
			"r|reverse=s"=>\$reverse,
			"o|out=s"=>\$prefix);
			
if ($help){print $MessAbruti; exit;}

#Prefix checking and output names

unless ($prefix)
{
	($prefix) = split /\.|_/, $forward;
	$prefix .= "Repaired";
}

my $outForward = $prefix."_1.fastq";
my $outReverse = $prefix."_2.fastq";
my $outSingle = $prefix."_single.fastq";

if($forward =~ m/\.gz$/)
{
	$outForward .= ".gz";
	$outReverse .= ".gz";
	$outSingle .= ".gz";
}

#Opening inputs and outputs

my ($fhInF, $fhInR, $fhOutF,$fhOutR, $fhSingle);

open $fhInF, "<", $forward or die ("\nCannot open $forward file:\n$!\n");
open $fhInR, "<", $reverse or die ("\nCannot open $reverse file:\n$!\n");

open $fhOutF, ">", $outForward  or die ("\nCannot create $outForward file:\n$!\n");
open $fhOutR, ">", $outReverse  or die ("\nCannot create $outReverse file:\n$!\n");
open $fhSingle, ">", $outSingle  or die ("\nCannot create $outSingle file:\n$!\n");

if($forward =~ m/\.gz$/)
    {
	#Transforming the output flux in a compressed gz format
	$fhOutR = new IO::Compress::Gzip $fhOutR or die ("\nCannot create the output gzip flux for $outReverse: $GzipError $!\n");
	$fhOutR->autoflush(1);
	$fhOutF = new IO::Compress::Gzip $fhOutF or die ("\nCannot create the output gzip flux for $outForward: $GzipError $!\n");
	$fhOutF->autoflush(1);
	$fhSingle = new IO::Compress::Gzip $fhSingle or die ("\nCannot create the output gzip flux for $outSingle: $GzipError $!\n");
	$fhSingle->autoflush(1);
	
	#Decompressing in input gz flux
	$fhInF = new IO::Uncompress::Gunzip $fhInF or die ("\nCannot open the input gzip flux for $forward: $GzipError $!\n");
	$fhInR = new IO::Uncompress::Gunzip $fhInR or die ("\nCannot open the input gzip flux for $reverse: $GzipError $!\n");
    }

#Picking up seq ID from the forward file
my %hashforward;
while (my $line = <$fhInF>)
	{
	my $next = $line;
	chomp $line;
	next if ($line =~ m/^$/);
	($line) = split /\/\d/, $line;
	($line) = split /\s/, $line;
	$next .= <$fhInF>;
	$next .= <$fhInF>;
	$next .= <$fhInF>;
	$hashforward{$line}=$next;
	}
close $fhInF;

#Comparing with the reverse seq ID
while (my $line = <$fhInR>)
	{
	my $next = $line;
	chomp $line;
	next if ($line =~ m/^$/);
	($line) = split /\/\d/, $line;
	($line) = split /\s/, $line;
	$next .= <$fhInR>;
	$next .= <$fhInR>;
	$next .= <$fhInR>; 
	#printing outputs for paired files
	if (exists $hashforward{$line})
		{
		my $out = $hashforward{$line};
		print $fhOutF $out;
		my $out2 = $next;
		print $fhOutR $out2;
		delete $hashforward{$line}; # To save memory and to conserve only the singles
		}
        #printing output for singles from reverse file
	else
		{
		my $out2 = $next;
		print $fhSingle $out2;
		}
	}

#printing output for singles from forward file
foreach my $remain (keys %hashforward)
	{
	my $out = $hashforward{$remain};
	print $fhSingle $out;
	}

#Closing files
close $fhInR;
close $fhOutF;
close $fhOutR;
close $fhSingle;

exit;

__END__