#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
NOM
       demulttag - demultiplexer les séquences fastq en fonction de
       leur adaptateur

SYNOPSIS
       python demulttag.py [OPTION] FILE_ADAPT

DESCRIPTION
       Sépare les séquences dans différents fichiers en fonction de leur
       adaptateur puis supprime l'adaptateur des séquences.
       
       ADAPTATEUR_FILE (requis)
            fichier avec la liste des adaptateur
            le format est :

                adaptateur_1    nom_de_l_individue_1
                adaptateur_2    nom_de_l_individue_2
                    [...]
                adaptateur_n    nom_de_l_individue_n
                *               indicidue_non_identifiable  

       OPTIONS

       -h
            show help
            
       -s  --single_end (N'est plus suporté)

       -l --levenshtein
            Use a Levenshtein distance to demultiple
            
       -f FASTQ_FILE (requis)
            fichier au format fastq


       -p PREFIX
            prefix des fichier de sortie. les fichiers de sortie sont au format
            prefix-nom_de_l_individue.fastq par default, le prefix est le nom
            du fichier FASTQ_FILE sans l'extention
"""

import sys, os
from davem_fastq import Fastq_read, Fastq_file
import argparse
from itertools import izip
from bisect import bisect_left
 
class FastqFileType( object ) :
    """
    Fabrique Fastq_file
    """    
    def __init__( self, mode ) :
        self.mode = mode
    
    def __call__( self, path_name) :
        return Fastq_file( path_name, self.mode )


class Selector( object ) :
    """
    Abstract class pour rechercher une ligne dans une table_adaptator.

    table_adaptator est de la forme :
       [ (adaptator-1, output-file-A1, output-file-B1),
         (adaptator-2, output-file-A2, output-file-B2),
                ...
         (adaptator-N, output-file-AN, output-file-BN)
        ]

    En single-end, la table_adaptator ne possède que un seul output file par
    tuple.

    Vous devez implementer les méthodes __single_select et __paired_select.
    """
    def __init__(self, table_adaptator, single_end) :
        """
        Si single_end est True, un appèle à monSelector.select( sequence )
        executera la méthode _single_select sinon l'appel sera monSelector.select( sequence-1, sequence-2 )
        et executera la méthode _paired_select
        """
        self.table_adaptator = table_adaptator
        if single_end :
            self.select = self._single_select
        else :
            self.select = self._paired_select

    def _single_select( self, sequence ) :
        """
        Rechercher une ligne dans la table_adaptator
        avec une seul sequence.
        """
        raise NotImplementedError

    def _paired_select( self, sequence_1, sequence_2 ) :
        """
        Rechercher une ligne dans la table_adaptator
        avec deux sequences.
        """
        raise NotImplementedError


class Levenshtein_selector( Selector ) :
    table_adaptator = None
    single_end = False
    rate = 0
    def __init__( self, table_adaptator, single_end, rate ) :
        if not isinstance( rate, float ) :
            raise ValueError( "rate argument must be a float not %s" % type( rate ) )
        Selector.__init__( self, table_adaptator, single_end)
        self.rate = rate

    def _single_select( self, sequence) :
        from Levenshtein import ratio
        
        distances = []
        for (adaptator, output_file) in self.table_adaptator :
            dist = ratio( adaptator, sequence[ : len( adaptator ) ] )
            if dist == 1.0 :
                return (adaptator, output_file)
        
            distances.append( dist )
        
        max_dist = max( distances )
        if max_dist >= self.rate and distances.count( max_dist ) == 1 :
            return self.table_adaptator[ distances.index( max_dist ) ]
        
        return None

    def _paired_select( self, sequence_1, sequence_2) :
        from Levenshtein import ratio
        distances_1 = []
        distances_2 = []

        for line in self.table_adaptator :
            adaptator = line[ 0 ]
            dist_1 = ratio( adaptator, sequence_1[ : len( adaptator ) ] )
            dist_2 = ratio( adaptator, sequence_2[ : len( adaptator ) ] )        
            distances_1.append( dist_1 )
            distances_2.append( dist_2 )

        max_dist_1 = max( distances_1 )
        max_dist_2 = max( distances_2 )

        if max_dist_1 > max_dist_2 :
            if max_dist_1 >= self.rate and distances_1.count( max_dist_1 ) == 1 :
                return self.table_adaptator[ distances_1.index( max_dist_1 ) ]
                                
        elif max_dist_1 < max_dist_2 :
            if max_dist_2 >= self.rate and distances_2.count( max_dist_2 ) == 1 :
                return self.table_adaptator[ distances_2.index( max_dist_2 ) ]

        else :
            if max_dist_1 >= self.rate :
                if distances_1.count( max_dist_1 ) == 1 :
                    index_1 = distances_1.index( max_dist_1 )
                    index_2 = distances_2.index( max_dist_2 )
                    if index_1 == index_2 :
                        return self.table_adaptator[ index_1 ]

                elif distances_2.count( max_dist_2 ) == 1 :
                    index_1 = distances_1.index( max_dist_1 )
                    index_2 = distances_2.index( max_dist_2 )
                    if index_1 == index_2 :
                        return self.table_adaptator[ distances_2.index( max_dist_2 ) ]

        return None


class LevenshteinAllSelector( Levenshtein_selector ) :
    """
    Identique à Levenshtein_selector sauf que en paired-end, les deux membre de la paire 
    doivent dépacer ou être égale au ratio min et l'adaptateur de deux membre doivent être identique.
    """

    def _paired_select( self, sequence_1, sequence_2) :
        from Levenshtein import ratio
        distances_1 = []
        distances_2 = []

        for line in self.table_adaptator :
            adaptator = line[ 0 ]
            dist_1 = ratio( adaptator, sequence_1[ : len( adaptator ) ] )
            dist_2 = ratio( adaptator, sequence_2[ : len( adaptator ) ] )        
            distances_1.append( dist_1 )
            distances_2.append( dist_2 )

        max_dist_1 = max( distances_1 )
        max_dist_2 = max( distances_2 )

        if ( max_dist_1 >= self.rate and max_dist_2 >= self.rate 
           and distances_1.count( max_dist_1 ) == distances_2.count( max_dist_2 ) == 1 ) :
               adapt_1 = self.table_adaptator[ distances_1.index( max_dist_1 ) ] 
               adapt_2 = self.table_adaptator[ distances_2.index( max_dist_2 ) ]
               if adapt_1 == adapt_2 :
                    return adapt_1
        else :
            return None

class Std_selector( Selector ):
    """
    Recherche dicotomique dans la list_adaptator
    table_adaptator
    Si l'index donnée est vide, None est retourné
    """

    def _paired_select( self, sequence_1, sequence_2):
        l1 = self._single_select( sequence_1 )
        l2 = self._single_select( sequence_2 )
        if l1 is None :
            return l2
        
        if l2 is None :
            return l1
        
        if l1 == l2 :
            return l1

        return None
        
        
    def _single_select( self, sequence):
        a = 0
        b = len( self.table_adaptator ) -1
        if b == -1 :
            return None
        
        while a <= b  :
            m = ( a + b ) // 2
            adaptator = self.table_adaptator[ m ][ 0 ]
            start_seq = sequence[ : len( adaptator ) ]
            
            if adaptator > start_seq :
                b = m - 1
            elif adaptator < start_seq :
                a = m + 1
            else :
                return self.table_adaptator[ m ]

        if adaptator == sequence[ : len( adaptator ) ] :
            return self.table_adaptator[ m ]
        return None


def get_adapt_counter( opened_adapt_file ) :
    """
    retourne un dictionnaire ou les clefs sont les adaptateurs et
    les valeur initialisé à [ name_tag, 0 ]
    """
    d = {}
    opened_adapt_file.seek(0)
    for line in opened_adapt_file :
        if not line.isspace() :
            try :    
                adapt, name_tag = line.split()
            except ValueError :
                print >> sys.stderr, "Le fichier '%s' est mal formée." %  opened_adapt_file.name
                exit( 1 )
            d[ adapt ] = [ name_tag, 0 ]
    return d


def get_maximal_annalogie( file_adapt ) :
    """
    Calcule le ratio Levenshtein maximal entre tous les adaptateurs
    """
    from Levenshtein import ratio
    adaptators = []
    for line in file_adapt :
        if line :
            (adapt, name) = line.split()
            if adapt != "*" :           
                adaptators.append( adapt )
   
    ratio_max = 0.0
    for i, adapt in enumerate( adaptators ) :
        for adapt2 in adaptators[i+1:] :
            ratio_max = max( ratio_max,ratio( adapt, adapt2 ) )

    return ratio_max




def get_output_files( opened_adapt_file, prefix, paired_end=True ) :
    """
    Créer les fichiers de sortie et les met dans une liste au format :
    
    si paired_end est a True, deux fichier sont créer par adaptateur
        [ (adaptateur, output_file_1, output_file_2 ), ...  ]
    
    sinon un seul
        [ (adaptateur, output_file ), ...  ]
        
    la fonction retourne la table des fichiers plus le(s) fichier(s) de rebut.
    2 fichier de rebut pour le paired-end 1 pour le single.
      ( table, (file-rebut, ) )
    """ 
    

    ada_files = []
    default = None
    cache_name_file_by_adapt = {}

    for line in opened_adapt_file :
        if not line.isspace() :
                try :    
                    adapt, suffix_file = line.split()
                except ValueError :
                    print >> sys.stderr, "Le fichier '%s' est mal formée." %  opened_adapt_file.name
                    exit( 1 )

                if paired_end :
                    if line[0] == '*' :
                        default = ( Fastq_file( "%s-%s_1.fastq" % (prefix, suffix_file), "w" ),
                                    Fastq_file( "%s-%s_2.fastq" % (prefix, suffix_file), "w" ), )

                    else :
                        if suffix_file in cache_name_file_by_adapt :
                            f1, f2 = cache_name_file_by_adapt[ suffix_file ]
                            ada_files.append( ( adapt, f1, f2 ) )

                        else :
                            f1 = Fastq_file( "%s-%s_1.fastq" % (prefix, suffix_file), "w" )
                            f2 = Fastq_file( "%s-%s_2.fastq" % (prefix, suffix_file), "w" )
                            ada_files.append( (adapt, f1, f2) )
                            cache_name_file_by_adapt[ suffix_file ] = (f1, f2)


                else :
                    # TODO faire le system de cache pour le mode single.
                    if line[0] == '*' :
                        default = ( Fastq_file( "%s-%s.fastq" % (prefix, suffix_file), "w" ) , )

                    else :
                        ada_files.append(  (
                                                adapt, 
                                                Fastq_file( "%s-%s.fastq" % (prefix, suffix_file), "w" )
                                           )
                        )

    if default is None :
        print >> sys.stderr, "Le fichier '%s' n'a pas de ligne avec le tag jocker *.\nAjouter une ligne '*    tag_name'." %  opened_adapt_file.name
        sys.exit(1)
    
    ada_files.sort()
    return ada_files, default


def parse_user_argument() :
    """
    Récupére les arguments que l'utilisateur
    à passé en ligne de commande.
    """
    parser = argparse.ArgumentParser( description="demultiplex fastq_file" )
    parser.add_argument( 'file_adapt', metavar="FILE_ADAPT", nargs=1, type=argparse.FileType('r') )

    parser.add_argument( '-f', '--fastq_1', dest="fastq_1", type=FastqFileType( "r" ), action='store', 
                            help="Pour un fichier single-end ou pour le 1er fichier paired-end " )

    parser.add_argument( '-F', '--fastq_2', dest="fastq_2", type=FastqFileType( "r" ), action='store', default=None,
                            help="Pour le 2éme fichier paired-end " )

    parser.add_argument( '-p', '--output_prefix', dest="output_prefix", default="", action='store',
                            help="Les fichiers de sortie ont pour nom: PREFIX-ADAPTATEUR.fastq"  )

    parser.add_argument( '-l', '--levenshtein', dest="levenshtein", action='store', type=float, default=None,
                            help="Use a Levenshtein distance to demultiple" )

    parser.add_argument( '-v', '--verbose', dest="verbose", action='store_true',
                            help="montre moi se que tu fait" )

    parser.add_argument( '-a', '--analogy', dest="analogy", action='store_true',
                            help="Compute the maximal Levenshtein ratio between adaptors" )

    parser.add_argument( '--all', dest="all", action='store_true',
                            help="Si cette option est active avec l'option levenshtein en paired-end, les deux membres de la paire doivent dépasser le ratio et chacun doit être proche d'un seul adapatateur. Si l'option levenshtein n'est pas actif, cette option est sans concéquence" )

    user_args = parser.parse_args()
    user_args.file_adapt = user_args.file_adapt[0]
    user_args.single_end = user_args.fastq_2 is None 
    return user_args

def main() :
    user_args = parse_user_argument()

    if user_args.analogy :
        print "Maximal Levenshtein ratio between adaptors is %f" % get_maximal_annalogie( user_args.file_adapt )
        sys.exit(0)        
            
    output_files_by_adapt, defaults_files = get_output_files( user_args.file_adapt,
                                                              user_args.output_prefix,
                                                              not user_args.single_end )

    nb_reads_writen = get_adapt_counter( user_args.file_adapt )

    user_args.file_adapt.close()

    if user_args.levenshtein : 
        if user_args.all :
            select_output_file = LevenshteinAllSelector( output_files_by_adapt, 
                                                       user_args.single_end,
                                                       user_args.levenshtein )

        else :
            select_output_file = Levenshtein_selector( output_files_by_adapt, 
                                                       user_args.single_end,
                                                       user_args.levenshtein )
    else :
        select_output_file = Std_selector( output_files_by_adapt, 
                                           user_args.single_end )

    # Single_end
    if user_args.single_end :
        default_file = defaults_files[0]
        for str_read in user_args.fastq_1 :
            read = Fastq_read( str_read )
            adapt_and_line = select_output_file.select( read.seq )
            if adapt_and_line is None :
                if user_args.verbose :
                    print "Read '%s' start with %s... and go to *" % (read.name, read.seq[ : 14 ])
                default_file.write( str( read ) )
                nb_reads_writen[ '*' ][ 1 ] += 1

            else :
                (adapt, output_file) = adapt_and_line
                if user_args.verbose :
                    print "Read '%s' start with %s... and go to %s" % (read.name, read.seq[ : len( adapt ) ], adapt)

                read.cut_start( len( adapt ) )
                output_file.write( str( read ) )
                nb_reads_writen[ adapt ][ 1 ] += 1

        user_args.fastq_1.close()

        for adapt, output_file in output_files_by_adapt :
            output_file.close()

    # Paired_end
    else :
        (default_file_1, default_file_2) = defaults_files

        for str_read_1, str_read_2 in izip( user_args.fastq_1, user_args.fastq_2 ) :
            read_1 = Fastq_read( str_read_1 )
            read_2 = Fastq_read( str_read_2 )

            adapt_and_line = select_output_file.select( read_1.seq, read_2.seq )
            
            if adapt_and_line is None :
                default_file_1.write( str( read_1 ) )
                default_file_2.write( str( read_2 ) )
                nb_reads_writen[ '*' ][1] += 1

            else :
                (adapt, output_file_1, output_file_2 ) = adapt_and_line

                read_1.cut_start( len( adapt ) )
                read_2.cut_start( len( adapt ) )

                output_file_1.write( str( read_1 ) )
                output_file_2.write( str( read_2 ) )
                nb_reads_writen[ adapt ][1] += 1

        user_args.fastq_1.close()
        user_args.fastq_2.close()

        for adapt, file_1, file_2 in output_files_by_adapt :
            file_1.close()
            file_2.close()

    # show stat.
    for nb_reads_by_name in nb_reads_writen.values() :
        print "%s %d reads" % tuple( nb_reads_by_name )


if __name__ == '__main__':
    main()
