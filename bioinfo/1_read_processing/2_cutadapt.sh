#!/bin/bash
#SBATCH -J cutadapt
#SBATCH --array=1-96
#SBATCH -e cutadapt-%a.err
#SBATCH -o cutadapt-%a.out
#SBATCH -p fast
#SBATCH --cpus-per-task 4
#SBATCH --mem 10G

module load cutadapt/3.1

file=$(ls /shared/projects/fonio_rawdata/results/demult/ | sed -n ${SLURM_ARRAY_TASK_ID}p)
cd /shared/projects/fonio_rawdata/results/demult/$file
cutadapt $(</shared/projects/fonio_rawdata/data/myadapters) -j 5 -O 5 -q 20 -m 35 --nextseq-trim 20 -o $file\_CU_1.fastq.gz -p $file\_CU_2.fastq.gz *_1.fastq.gz *_2.fastq.gz