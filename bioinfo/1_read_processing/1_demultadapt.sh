#!/bin/bash
#SBATCH -J demultadapt
#SBATCH --array=1-96
#SBATCH -e demult-%a.err
#SBATCH -o demult-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 3
#SBATCH --mem 20G

module load python/2.7

######## 1. demultadapt ########

#decompress R1 files
file=$(ls /shared/projects/fonio_rawdata/data/raw_reads/*/*1.fq.gz | sed -n ${SLURM_ARRAY_TASK_ID}p)
gzip -d $file

#demultadapt
file=$(ls /shared/projects/fonio_rawdata/data/raw_reads/*/*1.fq | sed -n ${SLURM_ARRAY_TASK_ID}p)
python /shared/projects/fonio_rawdata/source/demultiplex/demultadapt.py -f $file -p $file-DEMULT- /shared/projects/fonio_rawdata/source/demultiplex/FILE_ADAPT


######## 2. repairing ######## 
# decompress R2 files
file=$(ls /shared/projects/fonio_rawdata/results/demult/*/*2.fq.gz | sed -n ${SLURM_ARRAY_TASK_ID}p)
gzip -d $file


#repairing (R1 and R2 fastq files in the same folder)
file=$(ls /shared/projects/fonio_rawdata/data/raw_reads/ | sed -n ${SLURM_ARRAY_TASK_ID}p)
cd /shared/projects/fonio_rawdata/results/demult/$file
perl /shared/projects/fonio_rawdata/source/repairing2.pl -f $file\_1.fq -r $file\_2.fq -o $file

#use gzip to transform fq to fq.gz