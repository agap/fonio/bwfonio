#!/bin/bash
#SBATCH -J bamcaller
#SBATCH --array=1-18
#SBATCH -e bamcaller_iburua5-%a.err
#SBATCH -o bamcaller_iburua5-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G

#see tutorial https://github.com/popgenmethods/smcpp

module load bcftools/1.16
module load python/3.7
module load samtools/1.14

species="iburua5"
bamfiles="/shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/bam.${species}.filelist"
Chr=$(cat /shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/liste_chromosomes | sed -n ${SLURM_ARRAY_TASK_ID}p)

meandepth=$(cat /shared/projects/fonio_rawdata/results/first_dataset_VCF/smcpp/meanDepth/${species}.meandepth)
echo $meandepth

bcftools mpileup -q 10 -Q 20 -C 0 \
-r $Chr \
-f 04092019_Digitaria_Exilis_v1.1_pseudomolecules.fasta \
-b $bamfiles | bcftools call -c -V indels | python bamCaller.py $meandepth ${species}_${Chr}_mask.bed.gz | bgzip > ${species}_${Chr}.vcf.gz  

tabix -f -p vcf ${species}_${Chr}.vcf.gz

## The bed files created are for region sufficently covered
## We want a mask bed file for region not covered sufficiently

module load bedtools/2.30.0

bedtools complement -i ${species}_${Chr}_mask.bed.gz -g genome_size.txt -L | bgzip > ${species}_${Chr}_mask_complement.bed.gz
tabix -f -p bed ${species}_${Chr}_mask_complement.bed.gz
