#!/bin/bash
module load singularity
module load smcpp/1.15.4

species="longiflora5"
point1="100"
point2="30000"

for i in {1..30}
do
singularity exec  /shared/ifbstor1/software/singularity/images/smcpp-1.15.4.sif smc++ plot ${species}_estimates_${point1}_${point2}_thindefault_knots20_array${i}/plot_${species}_${point1}_${point2}_array${i}.pdf \
--verbose \
--csv \
-g 1 \
${species}_estimates_${point1}_${point2}_thindefault_knots20_array${i}/model.final.json
done