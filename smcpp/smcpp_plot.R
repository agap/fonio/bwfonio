### 08/03/2024
### plot demographic histories inferred with smc++
### Thomas Kaczmarek

library(ggplot2)
library(MASS) # to access Animals data sets
library(scales) # to access break formatting functions


#### import runs #####

#ternata
ternata5 <- list.files(path="01_results/02_first_snp_dataset/smcpp/ternata5_plots_30runs/",
                            pattern = ".csv",full.names = TRUE)

ternata53 <- list.files(path="01_results/02_first_snp_dataset/smcpp/ternata53_plots_30runs/",
                           pattern = ".csv",full.names = TRUE)

#iburua
iburua5 <- list.files(path="01_results/02_first_snp_dataset/smcpp/iburua5_plots_30runs/",
                       pattern = ".csv",full.names = TRUE)

#longiflora
longiflora5 <- list.files(path="01_results/02_first_snp_dataset/smcpp/longiflora5_plots_30runs/",
                      pattern = ".csv",full.names = TRUE)
longiflora54 <- list.files(path="01_results/02_first_snp_dataset/smcpp/longiflora54_plots_30runs/",
                          pattern = ".csv",full.names = TRUE)

#exilis
exilis5 <- list.files(path="01_results/02_first_snp_dataset/smcpp/exilis5_plots_30runs/",
                      pattern = ".csv",full.names = TRUE)

exilis52 <- list.files(path="01_results/02_first_snp_dataset/smcpp/exilis52_plots_30runs/",
                        pattern = ".csv",full.names = TRUE)

exilis53 <- list.files(path="01_results/02_first_snp_dataset/smcpp/exilis53_plots_30runs/",
                        pattern = ".csv",full.names = TRUE)


#### for two populations ####

smcplot_two_species <- function(species1,species2,color1,color2,xmax) {
  p <- ggplot() +
    theme_bw() +
    annotation_logticks()
  all_y1 <- matrix(nrow = 0,ncol = 101)
  all_y2 <- matrix(nrow = 0,ncol = 101)
  for (file in species1) {
    d1 <- read.table(file,header = TRUE,sep = ",")
    all_y1 <- rbind(all_y1,d1$y)
    p <- p + 
      geom_line(data=d1, aes(x=x,y=y), col=color1, linetype="dashed",
                alpha=0.3,linewidth=0.2) 
  }
  for (file in species2) {
    d2 <- read.table(file,header = TRUE,sep = ",")
    all_y2 <- rbind(all_y2,d2$y)
    p <- p + 
      geom_line(data=d2, aes(x=x,y=y), col=color2, linetype="dashed",
                alpha=0.3,linewidth=0.2) 
  }
  mean_y1 <- apply(all_y1,2,median)
  mean_data1 <- d1
  mean_data1$y <- mean_y1
  mean_y2 <- apply(all_y2,2,median)
  mean_data2 <- d2
  mean_data2$y <- mean_y2
  p <- p + geom_line(data=mean_data1, aes(x=x,y=y), col=color1, linetype="solid",
                     linewidth=1)
  p <- p + geom_line(data=mean_data2, aes(x=x,y=y), col=color2, linetype="solid",
                     linewidth=1)
  p <- p +
    scale_x_log10(breaks = trans_breaks("log10", function(x) 10^x),
                  labels = trans_format("log10", math_format(10^.x)),
                  limits=c(100,xmax)) +
    scale_y_log10(breaks = trans_breaks("log10", function(x) 10^x),
                  labels = trans_format("log10", math_format(10^.x))) +
    xlab("Time (in generation)") + ylab("Effective population size")
  p
  
}

smcplot_two_species(exilis5,iburua5,color1 = "darkorchid4",color2="grey",xmax=60000)


#### for three populations #### 

smcplot_three_species <- function(species1,species2,species3,color1,color2,color3) {
  p <- ggplot() +
    theme_bw() +
    annotation_logticks()
  all_y1 <- matrix(nrow = 0,ncol = 101)
  all_y2 <- matrix(nrow = 0,ncol = 101)
  all_y3 <- matrix(nrow = 0,ncol = 101)
  for (file in species1) {
    d1 <- read.table(file,header = TRUE,sep = ",")
    all_y1 <- rbind(all_y1,d1$y)
    p <- p + 
      geom_line(data=d1, aes(x=x,y=y), col=color1, linetype="dashed",
                alpha=0.3,linewidth=0.2) 
  }
  for (file in species2) {
    d2 <- read.table(file,header = TRUE,sep = ",")
    all_y2 <- rbind(all_y2,d2$y)
    p <- p + 
      geom_line(data=d2, aes(x=x,y=y), col=color2, linetype="dashed",
                alpha=0.3,linewidth=0.2) 
  }
  for (file in species3) {
    d3 <- read.table(file,header = TRUE,sep = ",")
    all_y3 <- rbind(all_y3,d3$y)
    p <- p + 
      geom_line(data=d3, aes(x=x,y=y), col=color3, linetype="dashed",
                alpha=0.3,linewidth=0.2) 
  }
  mean_y1 <- apply(all_y1,2,median)
  mean_data1 <- d1
  mean_data1$y <- mean_y1
  mean_y2 <- apply(all_y2,2,median)
  mean_data2 <- d2
  mean_data2$y <- mean_y2
  mean_y3 <- apply(all_y3,2,median)
  mean_data3 <- d3
  mean_data3$y <- mean_y3
  p <- p + geom_line(data=mean_data1, aes(x=x,y=y), col=color1, linetype="solid",
                     linewidth=1)
  p <- p + geom_line(data=mean_data2, aes(x=x,y=y), col=color2, linetype="solid",
                     linewidth=1)
  p <- p + geom_line(data=mean_data3, aes(x=x,y=y), col=color3, linetype="solid",
                     linewidth=1)
  p <- p +
    scale_x_log10(breaks = trans_breaks("log10", function(x) 10^x),
                  labels = trans_format("log10", math_format(10^.x)),
                  limits=c(100,100000)) +
    scale_y_log10(breaks = trans_breaks("log10", function(x) 10^x),
                  labels = trans_format("log10", math_format(10^.x))) +
    xlab("Time (in generation)") + ylab("Effective population size")
  p
  
}


smcplot_three_species(ternata5,ternata53,iburua5,
                      color1="#F507D1",color2="darkblue",color3="black")

#### Four populations, two for exilis, two for longiflora ####
smcplot_four_species <- function(species1,species2,
                                 species3,species4,
                                 color1,color2,
                                 color3,color4) {
  p <- ggplot() +
    theme_bw() +
    annotation_logticks()
  all_y1 <- matrix(nrow = 0,ncol = 101)
  all_y2 <- matrix(nrow = 0,ncol = 101)
  all_y3 <- matrix(nrow = 0,ncol = 101)
  all_y4 <- matrix(nrow = 0,ncol = 101)
  for (file in species1) {
    d1 <- read.table(file,header = TRUE,sep = ",")
    all_y1 <- rbind(all_y1,d1$y)
    p <- p + 
      geom_line(data=d1, aes(x=x,y=y), col=color1, linetype="dashed",
                alpha=0.2,linewidth=0.2) 
  }
  for (file in species2) {
    d2 <- read.table(file,header = TRUE,sep = ",")
    all_y2 <- rbind(all_y2,d2$y)
    p <- p + 
      geom_line(data=d2, aes(x=x,y=y), col=color2, linetype="dashed",
                alpha=0.2,linewidth=0.2) 
  }
  for (file in species3) {
    d3 <- read.table(file,header = TRUE,sep = ",")
    all_y3 <- rbind(all_y3,d3$y)
    p <- p + 
      geom_line(data=d3, aes(x=x,y=y), col=color3, linetype="dashed",
                alpha=0.2,linewidth=0.2) 
  }
  for (file in species4) {
    d4 <- read.table(file,header = TRUE,sep = ",")
    all_y4 <- rbind(all_y4,d4$y)
    p <- p + 
      geom_line(data=d4, aes(x=x,y=y), col=color4, linetype="dashed",
                alpha=0.2,linewidth=0.2) 
  }
  mean_y1 <- apply(all_y1,2,median)
  mean_data1 <- d1
  mean_data1$y <- mean_y1
  mean_y2 <- apply(all_y2,2,median)
  mean_data2 <- d2
  mean_data2$y <- mean_y2
  mean_y3 <- apply(all_y3,2,median)
  mean_data3 <- d3
  mean_data3$y <- mean_y3
  mean_y4 <- apply(all_y4,2,median)
  mean_data4 <- d4
  mean_data4$y <- mean_y4
  p <- p + geom_line(data=mean_data1, aes(x=x,y=y), col=color1, linetype="solid",
                     linewidth=1)
  p <- p + geom_line(data=mean_data2, aes(x=x,y=y), col=color2, linetype="solid",
                     linewidth=1)
  p <- p + geom_line(data=mean_data3, aes(x=x,y=y), col=color3, linetype="solid",
                     linewidth=1)
  p <- p + geom_line(data=mean_data4, aes(x=x,y=y), col=color4, linetype="solid",
                     linewidth=1)
  p <- p +
    scale_x_log10(breaks = trans_breaks("log10", function(x) 10^x),
                  labels = trans_format("log10", math_format(10^.x)),
                  limits=c(100,60000)) +
    scale_y_log10(breaks = trans_breaks("log10", function(x) 10^x),
                  labels = trans_format("log10", math_format(10^.x))) +
    xlab("Time (in generation)") + ylab("Effective population size")
  p
  
}

smcplot_four_species(longiflora54,longiflora5,
                     exilis52,exilis53,
                     color1="orange",color2="#F5F226",
                     color3="grey",color4="darkorchid4")