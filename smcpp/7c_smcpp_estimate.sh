#!/bin/bash
#SBATCH -J smcestimate
#SBATCH --array=1-30
#SBATCH -e estimate_longiflora54_thindefault-%a.err
#SBATCH -o estimate_longiflora54_thindefault-%a.out
#SBATCH -p long 
#SBATCH --cpus-per-task 2
#SBATCH --mem 60G


# see tutorial https://github.com/popgenmethods/smcpp


module load singularity
module load python/3.7

species="longiflora54"
point1="100"
point2="30000"
thinning="default"
knots="20"

mkdir -p ${species}_estimates_${point1}_${point2}_thin${thinning}_knots${knots}_array${SLURM_ARRAY_TASK_ID}

a=$RANDOM
b=1`date +%N`
c=${SLURM_ARRAY_TASK_ID}
let "d=$a+$b+$c"


## add --thin for changing default value for thinning

singularity exec $HOME/smcpp_latest.sif smc++ estimate -o ${species}_estimates_${point1}_${point2}_thin${thinning}_knots${knots}_array${SLURM_ARRAY_TASK_ID} \
--knots $knots \
--spline piecewise \
--timepoints $point1 $point2 \
--cores 2 \
--seed $d \
6.5e-9 \
/shared/projects/fonio_rawdata/results/first_dataset_VCF/smcpp/${species}_*.smc.gz

echo "finished"