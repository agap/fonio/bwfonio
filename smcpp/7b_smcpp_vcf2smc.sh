#!/bin/bash
#SBATCH -J smcpp
#SBATCH --array=1-18
#SBATCH -e smcppexilis3-%a.err
#SBATCH -o smcppexilis3-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 10G

# see tutorial: https://github.com/popgenmethods/smcpp

module load singularity

species="exilis3"
Chr=$(cat /shared/projects/fonio_rawdata/results/first_dataset_VCF/modelisation/liste_chromosomes | sed -n ${SLURM_ARRAY_TASK_ID}p)
input="${species}_${Chr}.vcf.gz"
mask="${species}_${Chr}_mask_complement.bed.gz"
pop=$(cat ${species}List.csv)
ind=$(cat ${species}Ind)

echo $input
echo $mask
echo $pop
echo $ind

for i in $ind
do
singularity exec $HOME/smcpp_latest.sif smc++ vcf2smc -m $mask \
-d $i $i \
$input \
${species}_${Chr}_${i}.smc.gz \
$Chr $pop 
done