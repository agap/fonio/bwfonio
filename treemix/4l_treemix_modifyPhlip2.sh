### The output from Phylip will be modified because:
### 1) TreeMix accepts trees with only one line
### 2) TreeMix accepts newick format file 

GenericName="4species"
###noroot
for ((m=0;m<=2; m++))
    do
        cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/${GenericName}/noRoot/m${m}
        cat outtree | tr -d "\n" > ${GenericName}_noroot_m${m}_finalconstree.newick
        echo >> ${GenericName}_noroot_m${m}_finalconstree.newick
        echo "***** Phylip - consensus tree construction: DONE *****"
done