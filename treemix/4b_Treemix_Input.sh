#!/bin/bash
#SBATCH -J TreeMixInput
#SBATCH -e TreeMixInput.err
#SBATCH -o TreeMixInput.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 10G

module load plink/1.90b6.18

#file to convert
file="/shared/projects/fonio_rawdata/results/first_dataset_VCF/allChr/all_chr_first_dataset_247samples_FILTERED_masking2DP_RemoveSite05missing_MAF005"

plink --file $file \
--remove /shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/4species/Removed \
--geno 0.05 \
--maf 0.01 \
--set-missing-var-ids @:# \
--allow-extra-chr \
--recode \
--out file_for_frequencies


plink --file /shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/4species/file_for_frequencies \
--nonfounders \
--freq \
--within /shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/4species/clustfile \
--set-missing-var-ids @:# \
--allow-extra-chr

rm plink.frq.strat.gz
gzip plink.frq.strat

/shared/projects/fonio_rawdata/source/plink2treemix.py plink.frq.strat.gz 4species.frq.gz