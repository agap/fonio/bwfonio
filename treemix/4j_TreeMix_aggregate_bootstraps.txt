### Create a file with all the bootstrapped trees
## No root
GenericName="4species"
cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/4species
for m in {0..2}
    do
        mkdir -p noRoot/m${m}
        rm noRoot/m${m}/${GenericName}_boottree*
        for a in {1..100}
            do
                 bootf="final_runs/bootstrap/noroot_constree_bootrep_${GenericName}_${m}_${a}.treeout.gz"
                 gunzip -c $bootf | head -1 >> noRoot/m${m}/${GenericName}_boottree_m${m}.tre
        done
done
echo "***** Bootstrap procedure: DONE *****"