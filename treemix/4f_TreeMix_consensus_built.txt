### The output from Phylip will be modified because:
### 1) TreeMix accepts trees with only one line
### 2) TreeMix accepts newick format file 
GenericName="4species"
DIR="/shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/${GenericName}"
file="${GenericName}"
cd ${DIR}/noRoot
cat outtree | tr -d "\n" > ${file}_constree.newick
echo >> ${file}_constree.newick
echo "***** Phylip - consensus tree construction: DONE *****"
