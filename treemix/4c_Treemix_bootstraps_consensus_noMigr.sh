#!/bin/bash
#SBATCH -J TreeMixBoot
#SBATCH --array=1-100 #number of bootstrap replicates of tree
#SBATCH -e TreeMixBoot-%a.err
#SBATCH -o TreeMixBoot-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 5G

module load treemix/1.13

### Generate bootstrap replicates

mkdir -p bootstrap

a=$RANDOM
b=1`date +%N`
let "c=$a+$b"

echo "Random seed = ${c}"

# Generate random k between 100 and 1000 in 100 SNP increments
k=$(seq 500 100 1000 | shuf -n 1)

#Treemix Input file
GenericName="4species"
TreemixInput="/shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/${GenericName}/${GenericName}.frq.gz"

#RUN TREEMIX WITH NO ROOT
treemix -i $TreemixInput \
-k ${k} \
-bootstrap \
-o bootstrap/noroot_${GenericName}_${SLURM_ARRAY_TASK_ID} \
-seed ${c}

