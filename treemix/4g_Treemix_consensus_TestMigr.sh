!/bin/bash
#SBATCH -J TreeMix
#SBATCH -e TreeMixEdges.err
#SBATCH -o TreeMixEdges.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 5G

mkdir -p test_migrations
module load treemix/1.13

### input file
GenericName="4species"
TreemixInput="/shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/${GenericName}/${GenericName}.frq.gz"
name="${GenericName}"

### NoRoot

mkdir -p test_migrations/noRoot

for ((m=0; m<=6; m++))
	do
		for ((i=1; i<=10; i++))
                do
			a=$RANDOM
			b=1`date +%N`
			let "c=$a+$b"

			echo "Random seed = ${c}"

			# Generate random k between 100 and 1000 in 100 SNP increments
			k=$(seq 500 100 1000 | shuf -n 1)
			 treemix \
			-i $TreemixInput \
                        -o test_migrations/noRoot/treemix.${m}.${i} \
                        -global \
                        -m ${m} \
                        -k ${k} \
			-seed ${c} \
			-tf noRoot/${name}_constree.newick 
	done 
done
