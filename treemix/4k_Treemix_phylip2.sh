#!/bin/bash
#SBATCH -J phylip
#SBATCH -e phylip.err
#SBATCH -o phylip.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 5G

module load phylip/3.697

folder="4species"
name="4species"


###NoRoot

for ((m=0;m<=2; m++))
	do
		cd /shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/$folder\/noRoot/m${m}
		rm -rf outfile outtree screanout
		echo $name\_boottree_m${m}.tre > $name\_m${m}_PhylipInputFile
		echo "Y" >> $name\_m${m}_PhylipInputFile
		phylip consense < $name\_m${m}_PhylipInputFile > screanout
done