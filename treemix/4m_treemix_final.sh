#!/bin/bash
#SBATCH -J TreeMix
#SBATCH --array=1-30 #number of independant runs
#SBATCH -e TreeMixFinal-%a.err
#SBATCH -o TreeMixFinal-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 4
#SBATCH --mem 20G

module load treemix/1.13


a=$RANDOM
b=1`date +%N`
let "c=$a+$b"

echo "Random seed = ${c}"

# Generate random k between 100 and 1000 in 100 SNP increments
k=$(seq 500 100 1000 | shuf -n 1)

GenericName="4species"
file="${GenericName}.frq.gz"
name="${GenericName}"

#RUN TREEMIX WITH NO ROOT
for ((m=0; m<=2; m++))
	do
		treemix -i $file \
		-k ${k} \
		-o final_runs/noroot_$name\_m${m}_${SLURM_ARRAY_TASK_ID} \
		-seed ${c} \
		-se \
		-tf noRoot/m${m}/$name\_noroot_m${m}_finalconstree.newick \
		-m ${m} \
		-global
done