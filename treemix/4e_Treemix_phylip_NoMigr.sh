#!/bin/bash
#SBATCH -J phylip
#SBATCH -e phylip.err
#SBATCH -o phylip.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 5G

module load phylip/3.697

GenericName="4species"

### SET DIR
DIR="/shared/projects/fonio_rawdata/results/first_dataset_VCF/TreeMix/${GenericName}"
file="${GenericName}"

###NoRoot

cd ${DIR}/noRoot
rm -rf outfile outtree screanout

echo ${file}_bootconstree.tre > ${file}_PhylipInputFile
echo "Y" >> ${file}_PhylipInputFile

phylip consense < ${file}_PhylipInputFile > screanout
