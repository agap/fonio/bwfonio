#!/bin/bash
#SBATCH -J TreeMix
#SBATCH --array=1-100 #number of bootstrap replicates of tree
#SBATCH -e TreeMixStep3-%a.err
#SBATCH -o TreeMixStep3-%a.out
#SBATCH -p fast 
#SBATCH --cpus-per-task 2
#SBATCH --mem 5G

module load treemix/1.13

mkdir -p final_runs
mkdir -p final_runs/bootstrap

a=$RANDOM
b=1`date +%N`
let "c=$a+$b"

echo "Random seed = ${c}"

GenericName="4species"
file="${GenericName}.frq.gz"
name="${GenericName}"

# Generate random k between 100 and 1000 in 100 SNP increments
k=$(seq 500 100 1000 | shuf -n 1)

#RUN TREEMIX WITH NO ROOT
for ((m=0; m<=2; m++))
	do
		treemix -i $file \
		-k ${k} \
		-bootstrap \
		-o final_runs/bootstrap/noroot_constree_bootrep_$name\_${m}_${SLURM_ARRAY_TASK_ID} \
		-seed ${c} \
		-se \
		-tf noRoot/$name\_constree.newick \
		-m ${m}
done